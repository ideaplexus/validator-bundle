<?php

namespace IPC\ValidatorBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Symfony\Component\Validator\Exception\MissingOptionsException;

class Chain extends Constraint
{
    public const OPTION_CONSTRAINTS   = 'constraints';
    public const OPTION_STOP_ON_ERROR = 'stopOnError';

    /**
     * @var Constraint[]
     */
    public $constraints = [];

    /**
     * @var bool
     */
    public $stopOnError = true;

    /**
     * @param array|null $options
     *
     * @throws ConstraintDefinitionException
     * @throws InvalidOptionsException
     * @throws MissingOptionsException
     */
    public function __construct($options = null)
    {
        $optionKeys = [
            'groups',
            self::OPTION_CONSTRAINTS,
            self::OPTION_STOP_ON_ERROR,
        ];

        if (\is_array($options) &&
            !array_intersect(array_keys($options), $optionKeys)
        ) {
            $options = [self::OPTION_CONSTRAINTS => $options];
        }

        parent::__construct($options);

        if (!\is_array($this->constraints)) {
            throw new ConstraintDefinitionException(
                'The option "constraints" is expected to be an array in constraint ' . __CLASS__
            );
        }

        foreach ($this->constraints as $constraint) {
            if (!$constraint instanceof Constraint) {
                $value = \is_object($constraint) ? \get_class($constraint) : \gettype($constraint);
                throw new ConstraintDefinitionException(
                    sprintf('The value "%s" is not an instance of Constraint in constraint %s', $value, __CLASS__)
                );
            }
            if ($constraint instanceof Valid) {
                throw new ConstraintDefinitionException(
                    'The constraint Valid cannot be nested inside constraint ' . __CLASS__
                );
            }
        }
    }

    /**
     * @return string
     */
    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }

    /**
     * @return array
     */
    public function getRequiredOptions(): array
    {
        return [self::OPTION_CONSTRAINTS];
    }
}
