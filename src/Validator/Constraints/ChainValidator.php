<?php

namespace IPC\ValidatorBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ChainValidator extends ConstraintValidator
{
    /**
     * {@inheritdoc}
     *
     * @return void
     */
    public function validate($value, Constraint $constraint): void
    {
        $group                  = $this->context->getGroup();
        $violationList          = $this->context->getViolations();
        $violationCountPrevious = $violationList->count();

        foreach ($constraint->constraints as $chainConstraint) {
            $chainConstraint->groups = array_unique(array_merge($constraint->groups, $chainConstraint->groups));

            $this->context
                ->getValidator()
                ->inContext($this->context)
                ->validate($value, $chainConstraint, $group);

            if ($constraint->stopOnError && (\count($violationList) !== $violationCountPrevious)) {
                return;
            }
        }
    }
}
