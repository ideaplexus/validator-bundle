<?php

namespace IPC\ValidatorBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class DateInFuture extends Constraint
{
    /**
     * @var string
     */
    public $message = 'The date lies in the past.';

    /**
     * @return string
     */
    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
