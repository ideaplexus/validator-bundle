<?php

namespace IPC\ValidatorBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class FloatVal extends Constraint
{
    /**
     * @var string
     */
    public $message = 'This value is not a valid float.';

    /**
     * @return string
     */
    public function getTargets(): string
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
