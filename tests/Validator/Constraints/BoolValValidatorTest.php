<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\ValidatorBundle\Validator\Constraints\BoolVal;
use IPC\ValidatorBundle\Validator\Constraints\BoolValValidator;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\BoolValValidator
 */
class BoolValValidatorTest extends AbstractValidatorTest
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new BoolVal();
        $this->validator  = new BoolValValidator();
    }

    /**
     * @return array
     */
    public function providerValidValues(): array
    {
        return [
            [false],
            [true],
            [0],
            [1],
            ['0'],
            ['1'],
        ];
    }

    /**
     * @return array
     */
    public function providerInvalidValues(): array
    {
        return [
            ['a'],
            ['2'],
            ['true'],
            ['false'],
            [-1],
            [2],
            [0.25],
            [[]],
            [new \stdClass()],
        ];
    }
}
