<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\ValidatorBundle\Validator\Constraints\Chain;
use IPC\ValidatorBundle\Validator\Constraints\FloatVal;
use IPC\ValidatorBundle\Validator\Constraints\IntVal;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Valid;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\MissingOptionsException;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\Chain
 */
class ChainTest extends TestCase
{
    /**
     * @var Chain
     */
    protected $constraint;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new Chain([Chain::OPTION_CONSTRAINTS => []]);
    }

    /**
     * @return void
     */
    public function testProperties(): void
    {
        $this->assertEquals(true, $this->constraint->stopOnError);
    }

    /**
     * @return array
     */
    public function provider__constructException(): array
    {
        return [
            [
                'options'        => null,
                'message'        => 'The options "constraints" must be set for constraint IPC\ValidatorBundle\Validator\Constraints\Chain',
                'exceptionClass' => MissingOptionsException::class
            ],
            [
                'options' => [
                    Chain::OPTION_CONSTRAINTS => 'someValue',
                ],
                'message' => 'The option "constraints" is expected to be an array in constraint IPC\ValidatorBundle\Validator\Constraints\Chain',
            ],
            [
                'options' => [
                    Chain::OPTION_CONSTRAINTS => [
                        new \stdClass(),
                    ],
                ],
                'message' => 'The value "stdClass" is not an instance of Constraint in constraint IPC\ValidatorBundle\Validator\Constraints\Chain',
            ],
            [
                'options' => [
                    Chain::OPTION_CONSTRAINTS => [
                        new Valid(),
                    ],
                ],
                'message' => 'he constraint Valid cannot be nested inside constraint IPC\ValidatorBundle\Validator\Constraints\Chain',
            ],
        ];
    }

    /**
     * @param array|null  $options
     * @param string      $message
     * @param string|null $exceptionClass
     *
     * @dataProvider provider__constructException
     *
     * @covers ::__construct
     */
    public function test__constructException($options, $message, $exceptionClass = ConstraintDefinitionException::class): void
    {
        $this->expectException($exceptionClass);
        $this->expectExceptionMessage($message);

        new Chain($options);
    }

    /**
     * @return void
     *
     * @covers ::__construct
     */
    public function test__construct(): void
    {
        $options = [
            new FloatVal(),
        ];
        $chain = new Chain($options);
        $this->assertEquals($options, $chain->constraints);

        $options = [
            Chain::OPTION_CONSTRAINTS => [
                new IntVal(),
            ]
        ];
        $chain = new Chain($options);
        $this->assertEquals($options[Chain::OPTION_CONSTRAINTS], $chain->constraints);
    }

    /**
     * @return void
     *
     * @covers ::getRequiredOptions
     */
    public function testGetRequiredOptions(): void
    {
        $this->assertEquals([Chain::OPTION_CONSTRAINTS], $this->constraint->getRequiredOptions());
    }

    /**
     * @return void
     *
     * @covers ::getTargets
     */
    public function testGetTargets(): void
    {
        $this->assertEquals(Constraint::PROPERTY_CONSTRAINT, $this->constraint->getTargets());
    }
}
