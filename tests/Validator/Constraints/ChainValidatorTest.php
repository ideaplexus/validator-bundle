<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\ValidatorBundle\Validator\Constraints\Chain;
use IPC\ValidatorBundle\Validator\Constraints\ChainValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\ConstraintValidatorFactory;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Context\ExecutionContextFactory;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Validator\RecursiveValidator;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\ChainValidator
 */
class ChainValidatorTest extends TestCase
{
    /**
     * @var ChainValidator
     */
    protected $validator;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->validator = new ChainValidator();
    }

    /**
     * @return void
     *
     * @covers ::validate
     */
    public function testValidate(): void
    {
        $value = 'test';
        $violationList = new ConstraintViolationList();

        $dummyConstraint = new class extends Constraint {
            public function validatedBy(): string
            {
                return 'dummyValidator';
            }
        };
        $dummyConstraintValidator = $this->createMock(ConstraintValidator::class);
        $dummyConstraintValidator
            ->expects($this->once())
            ->method('validate')
            ->with($value, $dummyConstraint);

        $contextFactory = $this->createMock(ExecutionContextFactory::class);
        $metadataFactory = $this->createMock(LazyLoadingMetadataFactory::class);
        $validatorFactory = $this->createMock(ConstraintValidatorFactory::class);
        $recursiveValidator = new RecursiveValidator($contextFactory, $metadataFactory, $validatorFactory);

        $validatorFactory
            ->expects($this->once())
            ->method('getInstance')
            ->with($dummyConstraint)
            ->willReturn($dummyConstraintValidator);

        $context = $this->createMock(ExecutionContextInterface::class);
        $context
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($recursiveValidator);

        $context
            ->expects($this->any())
            ->method('getGroup')
            ->willReturn('Default');

        $context
            ->expects($this->once())
            ->method('getViolations')
            ->willReturn($violationList);

        $constraint = new Chain([
            $dummyConstraint
        ]);

        $this->validator->initialize($context);
        $this->validator->validate($value, $constraint);

        $this->assertCount(0, $violationList);
    }

    /**
     * @return void
     *
     * @covers ::validate
     */
    public function testValidateWithViolation(): void
    {
        $value = 'test';
        $violationList = new ConstraintViolationList();

        $dummyConstraint = new class extends Constraint {
            public function validatedBy(): string
            {
                return 'dummyValidator';
            }
        };
        $dummyConstraintValidator = $this->createMock(ConstraintValidator::class);
        $dummyConstraintValidator
            ->expects($this->once())
            ->method('validate')
            ->with($value, $dummyConstraint)
            ->willReturnCallback(function() use ($violationList) {
                $violation = new ConstraintViolation(
                    'message',
                    'messageTemplate',
                    [],
                    'root',
                    'propertyPath',
                    'invalidValue'
                );
                $violationList->add($violation);
            });

        $contextFactory = $this->createMock(ExecutionContextFactory::class);
        $metadataFactory = $this->createMock(LazyLoadingMetadataFactory::class);
        $validatorFactory = $this->createMock(ConstraintValidatorFactory::class);
        $recursiveValidator = new RecursiveValidator($contextFactory, $metadataFactory, $validatorFactory);

        $validatorFactory
            ->expects($this->once())
            ->method('getInstance')
            ->with($dummyConstraint)
            ->willReturn($dummyConstraintValidator);

        $context = $this->createMock(ExecutionContextInterface::class);
        $context
            ->expects($this->once())
            ->method('getValidator')
            ->willReturn($recursiveValidator);

        $context
            ->expects($this->any())
            ->method('getGroup')
            ->willReturn('Default');

        $context
            ->expects($this->once())
            ->method('getViolations')
            ->willReturn($violationList);

        $constraint = new Chain([
            $dummyConstraint
        ]);

        $this->validator->initialize($context);
        $this->validator->validate($value, $constraint);

        $this->assertCount(1, $violationList);
    }
}
