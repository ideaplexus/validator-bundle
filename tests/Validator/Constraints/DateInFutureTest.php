<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\ValidatorBundle\Validator\Constraints\DateInFuture;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\DateInFuture
 */
class DateInFutureTest extends TestCase
{
    /**
     * @var DateInFuture
     */
    protected $constraint;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new DateInFuture();
    }

    /**
     * @return void
     *
     * @coversNothing
     */
    public function testProperties(): void
    {
        $this->assertEquals('The date lies in the past.', $this->constraint->message);
    }

    /**
     * @return void
     *
     * @covers ::getTargets
     */
    public function testGetTargets(): void
    {
        $this->assertEquals(Constraint::PROPERTY_CONSTRAINT, $this->constraint->getTargets());
    }
}
