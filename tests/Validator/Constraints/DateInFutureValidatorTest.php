<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\ValidatorBundle\Validator\Constraints\DateInFuture;
use IPC\ValidatorBundle\Validator\Constraints\DateInFutureValidator;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\DateInFutureValidator
 */
class DateInFutureValidatorTest extends AbstractValidatorTest
{
    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new DateInFuture();
        $this->validator  = new DateInFutureValidator();
    }

    /**
     * @return array
     */
    public function providerValidValues(): array
    {
        return [
            [new \DateTime()],
            [new \DateTime('+1 day')],
        ];
    }

    /**
     * @return array
     */
    public function providerInvalidValues(): array
    {
        return [
            ['a'],
            ['2'],
            [2],
            [0.25],
            [[]],
            [new \stdClass()],
            [date('Y-m-d', strtotime('+ 1 day'))],
            [new \DateTime('-1 day')],
        ];
    }
}
