<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\ValidatorBundle\Validator\Constraints\EqualProperties;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\ConstraintDefinitionException;
use Symfony\Component\Validator\Exception\MissingOptionsException;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\EqualProperties
 */
class EqualPropertiesTest extends TestCase
{
    /**
     * @var EqualProperties
     */
    protected $constraint;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new EqualProperties(['propertyOne', 'propertyTwo']);
    }

    /**
     * @return void
     *
     * @coversNothing
     */
    public function testProperties(): void
    {
        $this->assertEquals(false, $this->constraint->invert);
        $this->assertEquals(false, $this->constraint->skipNull);
    }

    /**
     * @return array
     */
    public function provider__constructException(): array
    {
        return [
            [
                'options' => null,
                'message' => 'The options "properties" must be set for constraint IPC\ValidatorBundle\Validator\Constraints\EqualProperties',
                'exceptionClass' => MissingOptionsException::class
            ],
            [
                'options' => [
                    EqualProperties::OPTION_PROPERTIES => 'someValue',
                ],
                'message' => 'The option "properties" is expected to be an array in constraint IPC\ValidatorBundle\Validator\Constraints\EqualProperties',
            ],
            [
                'options' => [
                    EqualProperties::OPTION_PROPERTIES => ['propertyOne'],
                ],
                'message' => 'The option "properties" requires two or more elements in constraint IPC\ValidatorBundle\Validator\Constraints\EqualProperties',
            ],
            [
                'options' => [
                    EqualProperties::OPTION_PROPERTIES => [
                        'propertyOne',
                        'propertyTwo',
                    ],
                    EqualProperties::OPTION_INVERT => '',
                ],
                'message' => 'invert has to be a boolean.',
            ],
            [
                'options' => [
                    EqualProperties::OPTION_PROPERTIES => [
                        'propertyOne',
                        'propertyTwo',
                    ],
                    EqualProperties::OPTION_INVERT => 1,
                ],
                'message' => 'invert has to be a boolean.',
            ],
            [
                'options' => [
                    EqualProperties::OPTION_PROPERTIES => [
                        'propertyOne',
                        'propertyTwo',
                    ],
                    EqualProperties::OPTION_SKIP_NULL => '',
                ],
                'message' => 'skipNull has to be a boolean.',
            ],
            [
                'options' => [
                    EqualProperties::OPTION_PROPERTIES => [
                        'propertyOne',
                        'propertyTwo',
                    ],
                    EqualProperties::OPTION_SKIP_NULL => 1,
                ],
                'message' => 'skipNull has to be a boolean.',
            ],
        ];
    }

    /**
     * @param array|null  $options
     * @param string      $message
     * @param string|null $exceptionClass
     *
     * @return void
     *
     * @dataProvider provider__constructException
     *
     * @covers ::__construct
     */
    public function test__constructException($options, $message, $exceptionClass = ConstraintDefinitionException::class): void
    {
        $this->expectException($exceptionClass);
        $this->expectExceptionMessage($message);

        new EqualProperties($options);
    }

    /**
     * @return void
     *
     * @covers ::__construct
     */
    public function test__construct(): void
    {
        $options = [
            EqualProperties::OPTION_PROPERTIES => ['propertyOne', 'propertyTwo'],
            EqualProperties::OPTION_INVERT     => true,
            EqualProperties::OPTION_SKIP_NULL  => true,
        ];

        $equalProperties = new EqualProperties($options);

        $this->assertEquals($options[EqualProperties::OPTION_PROPERTIES], $equalProperties->properties);
        $this->assertEquals($options[EqualProperties::OPTION_INVERT], $equalProperties->invert);
        $this->assertEquals($options[EqualProperties::OPTION_SKIP_NULL], $equalProperties->skipNull);
    }

    /**
     * @return void
     *
     * @covers ::getTargets
     */
    public function testGetTargets(): void
    {
        $this->assertEquals(Constraint::CLASS_CONSTRAINT, $this->constraint->getTargets());
    }

    /**
     * @return void
     *
     * @covers ::getRequiredOptions
     */
    public function testGetRequiredOptions(): void
    {
        $this->assertEquals([EqualProperties::OPTION_PROPERTIES], $this->constraint->getRequiredOptions());
    }
}
