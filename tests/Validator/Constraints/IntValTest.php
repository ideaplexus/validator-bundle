<?php

namespace IPC\Tests\ValidatorBundle;

use IPC\ValidatorBundle\Validator\Constraints\IntVal;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;

/**
 * @coversDefaultClass \IPC\ValidatorBundle\Validator\Constraints\IntVal
 */
class IntValTest extends TestCase
{
    /**
     * @var IntVal
     */
    protected $constraint;

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $this->constraint = new IntVal();
    }

    /**
     * @return void
     *
     * @coversNothing
     */
    public function testProperties(): void
    {
        $this->assertEquals('The value is not a valid integer.', $this->constraint->message);
    }

    /**
     * @return void
     *
     * @covers ::getTargets
     */
    public function testGetTargets(): void
    {
        $this->assertEquals(Constraint::PROPERTY_CONSTRAINT, $this->constraint->getTargets());
    }
}
