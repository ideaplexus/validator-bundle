<?php

namespace IPC\Tests\ValidatorBundle\_files;

class TestDateRangeClass
{
    protected $startDate;
    protected $endDate;
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
        return $this;
    }
    public function getStartDate()
    {
        return $this->startDate;
    }
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }
    public function getEndDate()
    {
        return $this->endDate;
    }
}
